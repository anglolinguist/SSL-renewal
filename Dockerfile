FROM alpine:latest

RUN apk --no-cache add bash git curl openssl openssh-client

RUN touch ~/.bashrc

RUN curl https://get.acme.sh | sh
